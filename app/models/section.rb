class Section < ApplicationRecord
  belongs_to :language, optional: true
  has_many :articles

  validates :language_id,
    presence: true

  validates :title,
    presence: true
end
