class ArticlesController < ApplicationController
  before_action :set_article, only: [:show, :edit, :update, :destroy]
  before_action :set_language, only: [:index, :new, :edit, :create, :update]
  before_action :set_section, only: [:new, :edit, :create, :update]
  before_action :authenticate_user

    def index
    if article_params[:language_id].present?
      @articles = Article.where(language_id: article_params[:language_id])
    else
      @articles = Article.all
    end
  end

  def show
    @items = web_scraping(@article.body)
  end

  def new
    @article = Article.new
    if params[:language_id]
      @article.attributes = { language_id: params[:language_id] }
    end
  end

  def edit
    @sections = Section.where(language_id: @article.language_id)
  end

  def create
    @article = Article.new(article_params)
    if @article.save
      redirect_to @article, notice: '記事が作成されました'
    else
      render :new
    end
  end

  def update
    if @article.update(article_params)
      redirect_to @article, notice: '記事が更新されました'
    else
      render :edit
    end
  end

  def destroy
    @article.destroy
    redirect_to articles_url, notice: '記事が削除されました'
  end

  private
    def set_article
      @article = Article.find(params[:id])
    end

    def set_language
      @languages = Language.all
    end

    def set_section
      if params[:language_id]
        # バリデーションによる再描画時に、セクションタイトルの値保持用の変数
        @@global_language = params[:language_id]
        @sections = Section.where(language_id: params[:language_id])
      else
        if @@global_language ||= nil
          @sections = Section.where(language_id: @@global_language)
        else
          @sections = {}
        end
      end
    end

    def article_params
      params.fetch(:article, {}).permit(:language_id, :section_id, :title, :body, :is_released)
    end
end
