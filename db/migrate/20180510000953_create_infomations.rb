class CreateInfomations < ActiveRecord::Migration[5.1]
  def change
    create_table :infomations do |t|
      t.string :title
      t.text :body
      t.boolean :is_opened, default: false

      t.timestamps
    end
  end
end
