class SessionsController < ApplicationController
  def create
    user = User.find_by(user_name: params[:session][:user_name])
    if user && user.authenticate(params[:session][:password])
      session[:user_id] = user.id
      flash[:success] = "ログインに成功しました"
      redirect_to portal_articles_path
    else
      flash.now[:danger] = "ユーザ名またはパスワードが違います"
      render :new
    end
  end

  def destroy
    session[:user_id] = nil
    flash[:success] = "ログアウトに成功しました"
    redirect_to login_path
  end
end
