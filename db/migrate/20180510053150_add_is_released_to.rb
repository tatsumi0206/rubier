class AddIsReleasedTo < ActiveRecord::Migration[5.1]
  def change
    add_column :articles, :is_released, :boolean, default: false
  end
end
