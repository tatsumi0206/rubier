class LanguagesController < ApplicationController
  before_action :set_language, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user

  def index
    @languages = Language.all
  end

  def show
  end

  def new
    @language = Language.new
  end

  def edit
  end

  def create
    @language = Language.new(language_params)
    if @language.save
      redirect_to @language, notice: 'プログラミング言語が作成されました'
    else
      render :new
    end
  end

  def update
    if @language.update(language_params)
      redirect_to @language, notice: 'プログラミング言語が更新されました'
    else
      render :edit
    end
  end

  def destroy
    @language.destroy
    redirect_to languages_url, notice: 'プログラミング言語が削除されました'
  end

  private
    def set_language
      @language = Language.find(params[:id])
    end

    def language_params
      params.require(:language).permit(:name)
    end
end
