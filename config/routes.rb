Rails.application.routes.draw do
  resources :articles do
    collection do
      get :portal
    end
  end
  resources :languages
  resources :sections
  resources :issues, only: [:index, :show] do
    collection do
      get :portal
    end
  end
  resources :infomations, except: [:edit] do
    collection do
      get :complete
    end
  end
  get 'login', to: 'sessions#new'
  post 'login', to: 'sessions#create'
  delete 'logout', to: 'sessions#destroy'
  root to: 'issues#portal'
end
