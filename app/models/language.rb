class Language < ApplicationRecord
  has_many :articles
  has_many :sections

  validates :name,
    presence: true
end
