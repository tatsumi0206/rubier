class CreateArticles < ActiveRecord::Migration[5.1]
  def change
    create_table :articles do |t|
      t.references :language, foreign_key: true
      t.references :section, foreign_key: true
      t.string :title
      t.text :body

      t.timestamps
    end
  end
end
