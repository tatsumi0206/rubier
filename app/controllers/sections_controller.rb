class SectionsController < ApplicationController
  before_action :set_section, only: [:show, :edit, :update, :destroy]
  before_action :set_language, only: [:new, :edit, :create, :update]
  before_action :authenticate_user

  def index
    @sections = Section.all
  end

  def show
  end

  def new
    @section = Section.new
  end

  def edit
  end

  def create
    @section = Section.new(section_params)
    if @section.save
      redirect_to @section, notice: 'セクションが作成されました'
    else
      render :new
    end
  end

  def update
    if @section.update(section_params)
      redirect_to @section, notice: 'セクションが更新されました'
    else
      render :edit
    end
  end

  def destroy
    @section.destroy
    redirect_to sections_url, notice: 'セクションが削除されました'
  end

  private
    def set_section
      @section = Section.find(params[:id])
    end

    def set_language
      @languages = Language.all
    end

    def section_params
      params.require(:section).permit(:language_id, :title)
    end
end
