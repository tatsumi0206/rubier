class CreateSections < ActiveRecord::Migration[5.1]
  def change
    create_table :sections do |t|
      t.references :language, foreign_key: true
      t.text :title

      t.timestamps
    end
  end
end
