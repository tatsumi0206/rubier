class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  helper_method :logged_in?

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  def logged_in?
    current_user
  end

  def authenticate_user
    if !logged_in?
      flash[:danger] = "ログインしてください"
      redirect_to login_path
    end
  end

  private
    def web_scraping(body)
      regular_expression = 'h4[id*=item]'
      pages = Nokogiri::HTML(body).css(regular_expression)
      html = Hash.new
      pages.each_with_index do |page, num|
        html.store(page.values.join, page.text)
      end
      return html
    end
end
