class InfomationsController < ApplicationController
  before_action :set_infomation, only: [:show, :destroy]
  before_action :authenticate_user, except: [:new, :create, :complete]

  def index
    @infomations = Infomation.all
  end

  def new
    @infomation = Infomation.new
  end

  def show
    if !@infomation.is_opened
      @infomation.toggle(:is_opened)
    end
    if !@infomation.update(infomation_params)
      flash[:danger] = "エラーが発生しました"
    end
  end

  def create
    @infomation = Infomation.new(infomation_params)
    if @infomation.save
      redirect_to complete_infomations_path
    else
      render :new
    end
  end

  def destroy
    @infomation.destroy
    redirect_to infomations_path
    flash[:success] = "削除が完了しました"
  end

  private
    def set_infomation
      @infomation = Infomation.find(params[:id])
    end

    def infomation_params
      params.fetch(:infomation, {}).permit(:title, :body, :is_opened)
    end
end
