class Article < ApplicationRecord
  belongs_to :language, optional: true
  belongs_to :section, optional: true

  validates :language_id,
    presence: true

  validates :section_id,
    presence: true

  validates :title,
    presence: true

  validates :body,
    presence: true

  validate :new_released, on: :create

  def new_released
    if is_released
      errors[:base] << "一度、プレビューしてから公開するを選択してください"
    end
  end

end
