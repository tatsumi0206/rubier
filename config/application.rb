require_relative 'boot'

require 'rails/all'

Bundler.require(*Rails.groups)

module Program
  class Application < Rails::Application
    config.load_defaults 5.1
    config.i18n.default_locale = :ja
    # レイアウト崩れ防止のため、バリデーションエラー時にdiv.field_with_errorsを表示させない
    config.action_view.field_error_proc = Proc.new do |html_tag, instance|
      html_tag
    end
  end
end
