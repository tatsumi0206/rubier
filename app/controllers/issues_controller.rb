class IssuesController < ApplicationController
  def index
    if params[:language_id].present?
      @article_sections = Article.select(:section_id).where(language_id: params[:language_id]).distinct
    else
      @article_sections = Article.select(:section_id).where(language_id: 1).distinct
    end
  end

  def show
    @article = Article.find(params[:id])
    @items = web_scraping(@article.body)
  end

  def portal
    @languages = Language.all
  end


end
